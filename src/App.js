import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from './component/Card';

function App() {
  return (
    <Card />
  );
}

export default App;
