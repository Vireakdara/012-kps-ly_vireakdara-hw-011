import React from 'react'
import { ListGroup } from "react-bootstrap";

export default function ResultItem(props) {
    const { number1, number2, output, mathSign} = props.output;
    return (
        <div>
            <ListGroup.Item key={props.index}>
                {number1} {mathSign} {number2} = {output}
            </ListGroup.Item>
        </div>
    )
}
