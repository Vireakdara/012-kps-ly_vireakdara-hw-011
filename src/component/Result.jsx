import React from 'react';
import { ListGroup } from "react-bootstrap";
import ResultItem from "./List";

export default function ResultList(props) {
    return (
        <div>
            <h4>Result History</h4>
            <ListGroup>
                {props.output.map((output, index) => (
                <ResultItem key={index} output={output} />
                ))}
            </ListGroup>
        </div>
    );
}
