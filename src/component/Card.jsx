import React, { Component } from 'react'
import {Card,Button,Form,Row} from 'react-bootstrap'
import './style.css'
import ResultList from "./Result";

export default class CardBox extends Component {
    constructor(props){
        super(props);
        this.state  = {
            number1:"",
            number2:"",
            mathSign: "+",
            output: [],
        };
    }
    btnClick = () => {
        const { number1, number2, mathSign } = this.state;
        if ( number1 == "" && number2== "" && !number1 || !number2 ) {
            alert("Please input valid number!");
        }else{ let output;
            switch (mathSign){
                case "+":
                    output = Number(number1) + Number(number2);
                    break;
                case "-":
                    output = Number(number1) - Number(number2);
                    break;  
                case "*":
                    output = Number(number1) * Number(number2);
                    break;
                case "/":
                    output = Number(number1) / Number(number2);
                    break;
                case "%":
                    output = Number(number1) % Number(number2);
                    break;
            }this.setState({
                output: [...this.state.output,{
                        output: output,
                        number1: this.state.number1,
                        number2: this.state.number2,
                        mathSign: this.state.mathSign,
                    },
                ],
            });
        }
    }

    eventHandle = (event) => this.setState({[event.target.name]: event.target.value });

    render() {
        return (
            <div className="container">
                <div className="row">
                <div className="col-md-6">
                    <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src="https://icons.iconarchive.com/icons/scafer31000/bubble-circle-3/512/Calculator-icon.png" />
                    <Card.Body>
                        <Form>
            
                                <Form.Control type="text" name="number1" value={this.state.number1} onChange={this.eventHandle} />
                                <Form.Label></Form.Label>
                                <Form.Control type="text" name="number2" value={this.state.number2} onChange={this.eventHandle} />
                                <Form.Label></Form.Label>
                                <Form.Control as="select" className="form" name="mathSign" onChange={this.eventHandle} >
                                    
                                    <option value="+">+ Plus</option>
                                    <option value="-">- Subtract</option>
                                    <option value="*">* Multiple</option>
                                    <option value="/">/ Divide</option>
                                    <option value="%">% Module</option>
                                    
                                </Form.Control>
                                <Form.Label></Form.Label>
                        </Form>
                        <Button variant="primary" onClick={this.btnClick}>Calculate</Button>
                        
                    </Card.Body>
                    </Card>
                </div>
                <div className="col-md-6">
                <ResultList output={this.state.output} />
                </div>
                </div>
            </div>
        )
    }
}
